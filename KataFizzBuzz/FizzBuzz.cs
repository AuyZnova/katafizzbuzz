﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataFizzBuzz
{
    public class FizzBuzz
    {
        public string Convert(int input)
        {
            if (input % 15 == 0) 
                return "FizzBuzz";
            
            if (input % 3 == 0)
                return "Fizz";

            if (input % 5 == 0)
                return "Buzz";
            
            else
                return input.ToString();             
        }
    }
}
