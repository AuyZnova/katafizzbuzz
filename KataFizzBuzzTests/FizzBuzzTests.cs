﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KataFizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace KataFizzBuzz.Tests
{
    [TestClass()]
    public class FizzBuzzTests
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        string actualResult;
        string expectedResult;

        [TestMethod()]
        public void Input1ShouldReturn1()
        {
            expectedResult = "1";
            actualResult = fizzBuzz.Convert(1);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input2ShouldReturn2()
        {
            expectedResult = "2";
            actualResult = fizzBuzz.Convert(2);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input3ShouldReturnFizz()
        {
            expectedResult = "Fizz";
            actualResult = fizzBuzz.Convert(3);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input4ShouldReturn4()
        {
            expectedResult = "4";
            actualResult = fizzBuzz.Convert(4);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input5ShouldReturnBuzz()
        {
            expectedResult = "Buzz";
            actualResult = fizzBuzz.Convert(5);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input6ShouldReturnFizz()
        {
            expectedResult = "Fizz";
            actualResult = fizzBuzz.Convert(6);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input7ShouldReturn7()
        {
            expectedResult = "7";
            actualResult = fizzBuzz.Convert(7);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input9ShouldReturnFizz()
        {
            expectedResult = "Fizz";
            actualResult = fizzBuzz.Convert(9);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input10ShouldReturnBuzz()
        {
            expectedResult = "Buzz";
            actualResult = fizzBuzz.Convert(10);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input12ShouldReturnFizz()
        {
            expectedResult = "Fizz";
            actualResult = fizzBuzz.Convert(12);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod()]
        public void Input15ShouldReturnFizzBuzz()
        {
            expectedResult = "FizzBuzz";
            actualResult = fizzBuzz.Convert(15);
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
